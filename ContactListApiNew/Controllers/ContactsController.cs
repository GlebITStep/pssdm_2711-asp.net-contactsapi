﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactListApiNew.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactListApiNew.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        private readonly ContactsDbContext context;

        public ContactsController(ContactsDbContext context)
        {
            this.context = context;
        }

        // GET api/contacts
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Contact>> GetContacts()
        {
            return context.Contacts;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public ActionResult<Contact> GetContactById(int id)
        {
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact == null)
                return NotFound();
            
            return contact;
        }

        // POST api/contacts
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<Contact> CreateContact(Contact contact)
        {
            if (ModelState.IsValid)
            {
                context.Contacts.Add(contact);
                context.SaveChanges();
                return Created($"api/contacts/{contact.Id}", contact);
            }
            return BadRequest();
        }

        // POST api/contacts/bulk
        [HttpPost("bulk")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public ActionResult<IEnumerable<Contact>> CreateContacts(IEnumerable<Contact> contacts)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    context.Contacts.AddRange(contacts);
                    context.SaveChanges();
                    return Created("api/contacts", contacts);
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT api/contacts/1
        [HttpPut("{id}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult<Contact> UpdateContact(int id, Contact contact)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    contact.Id = id;
                    context.Contacts.Update(contact);
                    context.SaveChanges();
                    return Created($"api/contacts/{contact.Id}", contact);
                }
                catch (Exception)
                {
                    return NotFound();
                }
            }
            return BadRequest();
        }

        // DELETE api/contacts/1
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public StatusCodeResult RemoveContact(int id)
        {
            var contact = context.Contacts.FirstOrDefault(x => x.Id == id);
            if (contact == null)
                return NotFound();
            
            context.Contacts.Remove(contact);
            context.SaveChanges();
            return NoContent();
        }
    }
}